package com.main.workspace.xmltransforming;

import javax.xml.transform.*;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;

public class XslTransformer {
    private final TransformerFactory factory;

    public XslTransformer() {
        factory =  TransformerFactory.newInstance();
    }

    public void process(Reader xmlFile, Reader xslFile, Writer output)
            throws TransformerException {
        process(new StreamSource(xmlFile), new StreamSource(xslFile), new StreamResult(output));
    }

    public void process(File xmlFile, File xslFile, Writer output)
            throws TransformerException {
        process(new StreamSource(xmlFile), new StreamSource(xslFile), new StreamResult(output));
    }

    public void process(File xmlFile, File xslFile, OutputStream out)
            throws TransformerException {
        process(new StreamSource(xmlFile), new StreamSource(xslFile), new StreamResult(out));
    }

    public void process(Source xml, Source xsl, Result result)
            throws TransformerException {
        try {
            Templates template = factory.newTemplates(xsl);
            Transformer transformer = template.newTransformer();
            transformer.transform(xml, result);
        } catch (TransformerException te) {
            throw new TransformerException(te.getMessageAndLocation());
        }
    }
}
