package com.main.workspace.xmltransforming;

import javax.swing.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;

public class DocumentPane extends JEditorPane {
    public static final String TEXT = "text/plain";
    public static final String HTML = "text/html";
    private boolean loaded = false;
    private String filename = "";

    public void setPage(URL url) {
        if (url == null) {
            System.err.println("URL is null.");
            return;
        }

        loaded = false;
        try {
            super.setPage(url);
            File file = new File(url.toURI());
            setFilename(file.getName());
            loaded = true;
        } catch (Exception e) {
            System.err.println("Unable to set page: " + url);
        }
    }

    public void setText(String text) {
        super.setText(text);
        setFilename("");
        loaded = true;
    }

    public void loadFile(String filename) {
        try {
            File file = new File("src/main/resources/" + filename);
            if (!file.exists()) {
                System.err.println("File does not exist: " + filename);
                return;
            }
            setPage(file.toURI().toURL());
        } catch (Exception e) {
            System.err.println("Unable to load file: " + filename);
        }
    }

    public void saveFile(String filename) {
        try {
            File file = new File(filename);
            FileWriter writer = new FileWriter(file);
            writer.write(getText());
            writer.close();
            setFilename(file.getName());
        } catch (IOException ioe) {
            System.err.println("Unable to save file: " + filename);
        }
    }

    public String getFilename() {
        return(filename);
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public boolean isLoaded() {
        return(loaded);
    }
}