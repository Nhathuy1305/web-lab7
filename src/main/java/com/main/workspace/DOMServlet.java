package com.main.workspace;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

@WebServlet(name = "DOMServlet", urlPatterns = {"/DOMServlet"})
public class DOMServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String formType = request.getParameter("formType");
        if ("bookForm".equals(formType)) {
            processBookRequest(request, response);
        } else if ("webClassForm".equals(formType)) {
            processRequest(request, response);
        }
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet DOMServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1><center>List of Students in Web Class</center></h1>");
            out.println("<center><table border=1 cellpadding=0 bgcolor=#FFFFFF></center>");
            out.println("<tr><td><b>Name</b></td> <td><b>ID</b></td> <td><b>DATE</b></td> <td><b>CITY</b></td> </tr>");

            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

            factory.setNamespaceAware(true);

            DocumentBuilder parser = factory.newDocumentBuilder();

            String filePath = getServletContext().getRealPath("WEB-INF/WebClass.xml");

            Document document = parser.parse(filePath);

            Node booklist = document.getDocumentElement();

            NodeList books = booklist.getChildNodes();

            int nBooks = books.getLength();
            for (int i = 0; i < nBooks; i++) {
                Node book = books.item(i);
                if (book.getNodeType() != Node.TEXT_NODE) {
                    out.println("<tr>");
                    printBook(book, out);
                    out.println("</tr>");
                }
            }

            out.println("</body>");
            out.println("</html>");

        } catch (ParserConfigurationException ex) {
            Logger.getLogger(DOMServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SAXException e) {
            throw new RuntimeException(e);
        }
    }

    private void printBook(Node book, PrintWriter out) {
        NamedNodeMap attributes = book.getAttributes();

        if (attributes != null) {
            NodeList childNodes = book.getChildNodes();
            String name = "";
            String id = "";
            String date = "";
            String city = "";
            for (int i = 0; i < childNodes.getLength(); i++) {
                Node child = childNodes.item(i);
                String nodeName = child.getLocalName();
                if (nodeName != null) {
                    switch (nodeName) {
                        case "name": {
                            NodeList children = child.getChildNodes();
                            Node dateNode = children.item(0);
                            if (dateNode.getNodeType() == Node.TEXT_NODE) {
                                name = dateNode.getNodeValue();
                            }
                            break;
                        }
                        case "idNum": {
                            NodeList children = child.getChildNodes();
                            Node dateNode = children.item(0);
                            if (dateNode.getNodeType() == Node.TEXT_NODE) {
                                id = dateNode.getNodeValue();
                            }
                            break;
                        }
                        case "date-of-birth": {
                            NodeList children = child.getChildNodes();
                            Node priceNode = children.item(0);
                            if (priceNode.getNodeType() == Node.TEXT_NODE) {
                                date = priceNode.getNodeValue();
                            }
                            break;
                        }
                        case "city": {
                            NodeList children = child.getChildNodes();
                            Node priceNode = children.item(0);
                            if (priceNode.getNodeType() == Node.TEXT_NODE) {
                                city = priceNode.getNodeValue();
                            }
                            break;
                        }
                    }
                }
            }
            out.print("<td>" + name + "</td>" + "<td>" + id + "</td>" +
                    "<td>" + date + "</td>" + "<td>" + city + "</td>");
        }
    }

    protected void processBookRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet DOMServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1><center>List of Books</center></h1>");
            out.println("<center><table border=1 cellpadding=0 bgcolor=#FFFFFF></center>");
            out.println("<tr><td><b>ISBN-10</b></td> <td><b>TITLE</b></td> <td><b>AUTHOR</b></td> <td><b>PUBLISHER</b></td>" +
                    "<td><b>DATE</b></td> <td><b>PRICE</b></td> </tr>");

            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

            factory.setNamespaceAware(true);

            DocumentBuilder parser = factory.newDocumentBuilder();

            String filePath = getServletContext().getRealPath("WEB-INF/book.xml");

            Document document = parser.parse(filePath);

            Node booklist = document.getDocumentElement();

            NodeList books = booklist.getChildNodes();

            int nBooks = books.getLength();
            for (int i = 0; i < nBooks; i++) {
                Node book = books.item(i);
                if (book.getNodeType() != Node.TEXT_NODE) {
                    out.println("<tr>");
                    getDetail(book, out);
                    out.println("</tr>");
                }
            }

            out.println("</body>");
            out.println("</html>");

        } catch (SAXException | ParserConfigurationException ex) {
            Logger.getLogger(DOMServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void getDetail(Node book, PrintWriter out) {
        NamedNodeMap attributes = book.getAttributes();

        if (attributes != null) {
            NodeList childNodes = book.getChildNodes();
            String isbn = "";
            String title = "";
            String author = "";
            String publisher = "";
            String date = "";
            String price = "";
            for (int i = 0; i < childNodes.getLength(); i++) {
                Node child = childNodes.item(i);
                String nodeName = child.getLocalName();
                if (nodeName != null) {
                    switch (nodeName) {
                        case "isbn": {
                            NodeList children = child.getChildNodes();
                            Node dateNode = children.item(0);
                            if (dateNode.getNodeType() == Node.TEXT_NODE) {
                                isbn = dateNode.getNodeValue();
                            }
                            break;
                        }
                        case "title": {
                            NodeList children = child.getChildNodes();
                            Node dateNode = children.item(0);
                            if (dateNode.getNodeType() == Node.TEXT_NODE) {
                                title = dateNode.getNodeValue();
                            }
                            break;
                        }
                        case "author": {
                            NodeList children = child.getChildNodes();
                            Node priceNode = children.item(0);
                            if (priceNode.getNodeType() == Node.TEXT_NODE) {
                                author = priceNode.getNodeValue();
                            }
                            break;
                        }
                        case "publisher": {
                            NodeList children = child.getChildNodes();
                            Node priceNode = children.item(0);
                            if (priceNode.getNodeType() == Node.TEXT_NODE) {
                                publisher = priceNode.getNodeValue();
                            }
                            break;
                        }
                        case "publicationDate": {
                            NodeList children = child.getChildNodes();
                            Node priceNode = children.item(0);
                            if (priceNode.getNodeType() == Node.TEXT_NODE) {
                                date = priceNode.getNodeValue();
                            }
                            break;
                        }
                        case "price": {
                            NodeList children = child.getChildNodes();
                            Node priceNode = children.item(0);
                            if (priceNode.getNodeType() == Node.TEXT_NODE) {
                                price = priceNode.getNodeValue();
                            }
                            break;
                        }
                    }
                }
            }
            out.print("<td>" + isbn + "</td>" + "<td>" + title + "</td>" +
                    "<td>" + author + "</td>" + "<td>" + publisher + "</td>" +
                    "<td>" + date + "</td>" + "<td>" + price + "</td>");
        }
    }
}
