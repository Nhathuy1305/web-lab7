<%@ page import="java.io.*, java.util.*, javax.xml.parsers.*, org.w3c.dom.*, org.apache.poi.ss.usermodel.*, org.apache.poi.xssf.usermodel.*" %>
<%@ page import="org.xml.sax.SAXException" %>

<%
    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    DocumentBuilder builder;
    try {
        builder = factory.newDocumentBuilder();
    } catch (ParserConfigurationException e) {
        throw new RuntimeException(e);
    }
    Document doc;
    try {
        String filePath = application.getRealPath("/WEB-INF/book.xml");
        doc = builder.parse(new File(filePath));
    } catch (SAXException e) {
        throw new RuntimeException(e);
    }
    doc.getDocumentElement().normalize();

    // Create Excel workbook and sheet
    XSSFWorkbook workbook = new XSSFWorkbook();
    XSSFSheet sheet = workbook.createSheet("Books");

    String[] headers = {"ISBN", "Title", "Author", "Publisher", "Publication Date", "Price"};
    XSSFRow headerRow = sheet.createRow(0);
    for (int i = 0; i < headers.length; i++) {
        XSSFCell cell = headerRow.createCell(i);
        cell.setCellValue(headers[i]);
    }

    NodeList nodeList = doc.getElementsByTagName("book");
    for (int i = 0; i < nodeList.getLength(); i++) {
        Node node = nodeList.item(i);
        if (node.getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) node;
            XSSFRow row = sheet.createRow(i + 1);
            row.createCell(0).setCellValue(element.getElementsByTagName("isbn").item(0).getTextContent());
            row.createCell(1).setCellValue(element.getElementsByTagName("title").item(0).getTextContent());
            row.createCell(2).setCellValue(element.getElementsByTagName("author").item(0).getTextContent());
            row.createCell(3).setCellValue(element.getElementsByTagName("publisher").item(0).getTextContent());
            row.createCell(4).setCellValue(element.getElementsByTagName("publicationDate").item(0).getTextContent());
            row.createCell(5).setCellValue(Double.parseDouble(element.getElementsByTagName("price").item(0).getTextContent()));
        }
    }

    response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
    response.setHeader("Content-Disposition", "attachment; filename=\"books.xlsx\"");
    OutputStream outputStream = response.getOutputStream();
    workbook.write(outputStream);
    workbook.close();
    outputStream.close();
%>
